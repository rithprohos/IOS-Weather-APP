//
//  WeatherCell.swift
//  WeatherApp
//
//  Created by Som Rith Prohos on 10/18/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var summaryLbl: UILabel!
    @IBOutlet weak var time: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func renderCell(dialyWeather : DailyWeather) {
        summaryLbl.text = dialyWeather.summary
        time.text = dialyWeather.time.getDateStringFromUTC()
    }

   
}
