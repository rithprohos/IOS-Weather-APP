//
//  DailyWeather.swift
//  WeatherApp
//
//  Created by Som Rith Prohos on 10/18/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import Foundation

class DailyWeather {
    private var _summary : String!
    private var _icon : String!
    private var _time : Double!
    
    var summary : String {
        get {
            return _summary
        }
        set {
            _summary = newValue
        }
    }
    
    var icon : String {
        get {
            return _icon
        }
        set {
            _icon = newValue
        }
    }
    
    var time: Double {
        get {
            return _time
        }
        set {
            _time = newValue
        }
    }
    
    init(summary : String , time : Double , icon : String) {
        _summary = summary
        _time = time
        _icon = icon
    }
}
