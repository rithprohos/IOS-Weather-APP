//
//  Cordination.swift
//  WeatherApp
//
//  Created by Som Rith Prohos on 10/17/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import Foundation

struct Coordination {
    var latitude : Double
    var longtitude : Double
 
}
