//
//  ViewController.swift
//  WeatherApp
//
//  Created by Som Rith Prohos on 10/17/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import SwiftMoment

class ViewController: UIViewController , CLLocationManagerDelegate{

    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var detailLbl: UILabel!
    
    
    let locationManager = CLLocationManager()
    let secretKey = "868623a6b825506455b6d2f52410d166"
    var coordinate = Coordination(latitude : 0, longtitude : 0)
    var dailyWeatherJSON : JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        
        //for use when the app is open & in the background
        locationManager.requestAlwaysAuthorization()
        
        // for use when the app is open
        //locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            coordinate.latitude = location.coordinate.latitude
            coordinate.longtitude = location.coordinate.longitude
        }
        
        forecastRequest(secretKey: secretKey, coordination: coordinate)
        
    }
    
    
    @IBAction func dailyForecastPressed(_ sender: Any) {
        
        performSegue(withIdentifier: "weatherVC", sender: dailyWeatherJSON)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DailyWeatherVC {
            if let weather = sender as? JSON {
                destination.weatherJSON = weather
            }
        }
    }
    
    
    func forecastRequest(secretKey : String,coordination : Coordination) {
        Alamofire.request("https://api.darksky.net/forecast/\(secretKey)/\(coordination.latitude),\(coordination.longtitude)").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                self.dailyWeatherJSON = swiftyJsonVar["daily"]
                self.detailLbl.text = (swiftyJsonVar["daily"]["summary"]).stringValue
                if swiftyJsonVar["daily"]["icon"] == "rain" {
                    self.weatherImage.image = #imageLiteral(resourceName: "drop")
                }
                
            //    print(self.dailyWeatherJSON!)
                    
            }
        }
    }
    

   


}

// convert unix timestap
extension Double {
    func getDateStringFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium
        
        return dateFormatter.string(from: date)
    }
}

