//
//  DailyWeatherVC.swift
//  WeatherApp
//
//  Created by Som Rith Prohos on 10/18/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DailyWeatherVC: UIViewController , UITableViewDelegate , UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    
    var dialyWeathers : [DailyWeather] = []
    var weatherJSON = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
       
        self.title = "Daily Weather"
        
        if let resObject = weatherJSON["data"].arrayObject {
            parseJSONToArrayModel(restData: resObject as! [[String : AnyObject]])
        }
        
        if dialyWeathers.count > 0 {
            print("have data")
            tableView.reloadData()
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as? WeatherCell {
            let weatherDaily = dialyWeathers[indexPath.row]
            cell.renderCell(dialyWeather: weatherDaily)
            return cell
        } else {
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dialyWeathers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    
    
    func dailyWeatherRequest(secretKey : String,coordination : Coordination) {
        Alamofire.request("https://api.darksky.net/forecast/\(secretKey)/\(coordination.latitude),\(coordination.longtitude)").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                if let resData = swiftyJsonVar["daily"]["summary"].arrayObject {
                    self.parseJSONToArrayModel(restData: resData as! [[String : AnyObject]])
                    
                }
                if self.dialyWeathers.count > 0 {
                    self.tableView.reloadData()
                }
            
                
            }
        }
    }
    
    func parseJSONToArrayModel(restData : [[String : AnyObject]]) {
        for index in 0 ..< restData.count {
            let dict = restData[index]
            let icon = dict["icon"] as? String
            let time = dict["time"] as? Double ?? 0
            let summary = dict["summary"] as? String
            self.dialyWeathers.append(DailyWeather(summary: summary!, time: time, icon: icon!))
        }
    }
    


}
